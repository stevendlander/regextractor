pub struct Identifier<'a> {
    name: &'a str,
    description: &'a str,
}

impl<'a> Identifier<'a> {
    pub fn new(name: &'a str, description: &'a str) -> Identifier<'a> {
        Identifier{ name, description }
    }
}

pub struct TextLocation {
    offset: u32,
    length: u32,
}

impl TextLocation {
    pub fn new(offset: u32, length: u32) -> TextLocation {
        TextLocation{ offset, length }
    }
}

pub struct ExtractedData<'a> {
    identifier: Identifier<'a>,
    value: String,
    location: TextLocation,
}

impl<'a> ExtractedData<'a> {
    pub fn bitcoin(value: &str, offset: u32, length: u32) -> ExtractedDataType {
        let id = Identifier::new("Bitcoin Address", "Bech32");
        let tl = TextLocation::new(0, 32);
        let ed = ExtractedData{ identifier: id, value: value.to_owned(), location: tl };

        ExtractedDataType::BitcoinAddress(ed)
    }
}

pub enum ExtractedDataType<'a> {
    BitcoinAddress(ExtractedData<'a>),
    IpAddress(ExtractedData<'a>),
}

pub enum ExtractedDataKind {
    BitcoinAddress,
    IpAddress,
    Hello,
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn id_init() {
        let name = "Bitcoin Address";
        let desc = "Bech32";
        let result = Identifier::new(name, desc);

        assert_eq!(result.name, name);
        assert_eq!(result.description, desc);
    }

    #[test]
    fn text_loc_init() {
        let offset = 0;
        let len = 5;

        let result = TextLocation::new(offset, len);

        assert_eq!(result.offset, offset);
        assert_eq!(result.length, len);
    }
}
