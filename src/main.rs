use std::fmt;
use regex::Regex;

mod data_types;

struct TextLocation {
    offset: usize,
    length: usize,
}

impl TextLocation {
    fn new(start: usize, end: usize) -> TextLocation {
        TextLocation {
            offset: start,
            length: end - start,
        }
    }
}

impl fmt::Display for TextLocation {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "TextLocation(offset={}, length={})", self.offset, self.length)
    }
}

struct Extraction {
    kind: data_types::ExtractedDataKind,
    location: TextLocation,
    value: String,
}

impl Extraction {
    fn new(kind: data_types::ExtractedDataKind, location: TextLocation, value: String) -> Extraction {
        Extraction {
            kind,
            location,
            value,
        }
    }
}

impl fmt::Display for Extraction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let kind = match self.kind {
            data_types::ExtractedDataKind::Hello => "Hello",
            data_types::ExtractedDataKind::BitcoinAddress => "BitcoinAddress",
            data_types::ExtractedDataKind::IpAddress => "IpAddress",
        };
        write!(f, "Extraction(kind={}, value={}, location={})", kind, self.value, self.location)
    }
}

fn main() {
    let s = String::from("Hello, world! hello");

    let pattern: Regex = Regex::new(r"[hH]ello").unwrap();
    for thing in get_matches(&s, &pattern) {
        println!("{thing}");
    }
}

fn get_matches(input: &str, pattern: &Regex) -> Vec<Extraction> {
    let mut results: Vec<Extraction> = Vec::new();
    for capture in pattern.captures_iter(input) {
        let v = capture.get(0).unwrap();
        let location = TextLocation::new(v.start(), v.end());
        let this_match = Extraction::new(
            data_types::ExtractedDataKind::Hello,
            location,
            String::from(v.as_str())
        );
        
        results.push(this_match);
    }
    results
}
